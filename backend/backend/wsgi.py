"""
WSGI config for backend project.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/5.0/howto/deployment/wsgi/
"""

import os
from django.core.wsgi import get_wsgi_application

# Determine the environment and set the settings module accordingly
ENVIRONMENT = os.getenv('DJANGO_ENV', default='dev')

if ENVIRONMENT == 'prod':
    settings_module = 'backend.settings.production'
elif ENVIRONMENT == 'staging':
    settings_module = 'backend.settings.staging'
else:
    settings_module = 'backend.settings.development'

os.environ.setdefault('DJANGO_SETTINGS_MODULE', settings_module)

application = get_wsgi_application()

