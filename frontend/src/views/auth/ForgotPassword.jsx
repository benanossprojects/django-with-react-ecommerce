import React, { useState } from "react";
import { useSearchParams, Link } from "react-router-dom";
import apiInstance from "../../utils/axios";
import { useNavigate } from "react-router-dom";

function ForgotPassword() {
  const [email, setEmail] = useState("");
  const axios = apiInstance;
  const [searchParams] = useSearchParams();
  const otp = searchParams.get("otp");
  const uuid = searchParams.get("uuid");
  const [isLoading, setIsloading] = useState(false)

  const navigate = useNavigate();

  // const handleEmailChange = (event) => {
  //     setEmail(event.target.value)
  //     console.log(email);
  // }

  // const handleEmailSubmit = () => {
  //     axios.get(`user/password-reset/${email}/`).then((res) => {
  //         console.log(res.data);
  //         Swal.fire({
  //             icon: 'success',
  //             title: 'Password Reset Email Sent!',
  //         })
  //     })
  // }

  const handleSubmit = async () => {
    setIsloading(true)
    try {
      await apiInstance.get(`user/password-reset/${email}/`).then((res) => {
        alert("An email has been sent to you");
        // navigate("/create-new-password");
        setIsloading(false)
      });
    } catch (error) {
      alert("Email does not exists");
      setIsloading(false)
    }
  };

  return (
    <div>
      <section>
        <main className="" style={{ marginBottom: 100, marginTop: 50 }}>
          <div className="container">
            {/* Section: Login form */}
            <section className="">
              <div className="row d-flex justify-content-center">
                <div className="col-xl-5 col-md-8">
                  <div className="card rounded-5">
                    <div className="card-body p-4">
                      <h3 className="text-center">Forgot Password</h3>
                      <br />

                      <div className="tab-content">
                        <div
                          className="tab-pane fade show active"
                          id="pills-login"
                          role="tabpanel"
                          aria-labelledby="tab-login"
                        >
                          <div>
                            {/* Email input */}
                            <div className="form-outline mb-4">
                              <label className="form-label" htmlFor="Full Name">
                                Email Address
                              </label>
                              <input
                                type="text"
                                id="email"
                                name="email"
                                className="form-control"
                                onChange={(e) => setEmail(e.target.value)}
                              />
                            </div>

                            {isLoading === true
                              ? <button disabled type="button" className='btn btn-primary w-100 mb-4'>Processing...</button>
                              : <button onClick={handleSubmit} type="button" className='btn btn-primary w-100 mb-4'>Send email <i className="fas fa-paper-plane"></i></button>
                            }
                            <div className="text-center">
                              <p>
                                Want to sign in? <Link to="/login">Login</Link>
                              </p>
                            </div>

                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </section>
          </div>
        </main>
      </section>
    </div>
  );
}

export default ForgotPassword;
